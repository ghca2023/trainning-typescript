import { Router, Request, Response } from "express";
import { MonitoramentoService } from "../service/service-monitor";
import { CreateMonitoramentoDto } from "../dto/create-monitor";
import { UpdateMonitoramentoDto } from "../dto/update-monitor";

const router = Router();
const monitoramentoService = new MonitoramentoService();

router.get("/monitoramentos", async (req: Request, res: Response) => {
  try {
    const monitoramentos = await monitoramentoService.findAll();
    return res.json(monitoramentos);
  } catch (error) {
    return res.status(500).json({ message: "Internal server error" });
  }
});

router.get("/monitoramentos/:id", async (req: Request, res: Response) => {
  try {
    const id = parseInt(req.params.id);
    const monitoramento = await monitoramentoService.findById(id);
    if (!monitoramento) {
      return res.status(404).json({ message: "Monitoramento not found" });
    }
    return res.json(monitoramento);
  } catch (error) {
    return res.status(500).json({ message: "Internal server error" });
  }
});

router.post("/monitoramentos", async (req: Request, res: Response) => {
  try {
    const monitoramentoData: CreateMonitoramentoDto = req.body;
    const monitoramento = await monitoramentoService.create(monitoramentoData);
    return res.json(monitoramento);
  } catch (error) {
    return res.status(500).json({ message: "Internal server error" });
  }
});

router.put("/monitoramentos/:id", async (req: Request, res: Response) => {
  try {
    const id = parseInt(req.params.id);
    const monitoramentoData: UpdateMonitoramentoDto = req.body;
    const updatedMonitoramento = await monitoramentoService.update(
      id,
      monitoramentoData
    );
    return res.json(updatedMonitoramento);
  } catch (error) {
    return res.status(500).json({ message: "Internal server error" });
  }
});

// DELETE /monitoramentos/:id
router.delete("/monitoramentos/:id", async (req: Request, res: Response) => {
  try {
    const id = parseInt(req.params.id);
    const monitoramento = await monitoramentoService.delete(id);
    return res.json(monitoramento);
  } catch (error) {
    return res.status(500).json({ message: "Internal server error" });
  }
});

export default router;
