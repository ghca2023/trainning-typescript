import { IsNumber, IsOptional } from "class-validator";

export class UpdateMonitoramentoDto {
  @IsOptional()
  @IsNumber()
  hdUsado?: number;

  @IsOptional()
  @IsNumber()
  cpuUso?: number;

  @IsOptional()
  @IsNumber()
  memUsada?: number;
}
