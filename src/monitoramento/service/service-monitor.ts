import { PrismaClient, Monitoramento } from "@prisma/client";
import { Request } from "express";
import { CreateMonitoramentoDto } from "../dto/create-monitor";
import { UpdateMonitoramentoDto } from "../dto/update-monitor";

const prisma = new PrismaClient();

export class MonitoramentoService {
  async create(data: CreateMonitoramentoDto): Promise<Monitoramento> {
    return prisma.monitoramento.create({ data });
  }

  async update(
    id: number,
    data: UpdateMonitoramentoDto
  ): Promise<Monitoramento> {
    return prisma.monitoramento.update({ where: { id }, data });
  }

  async findById(id: number): Promise<Monitoramento | null> {
    return prisma.monitoramento.findUnique({ where: { id } });
  }

  async findAll(): Promise<Monitoramento[]> {
    return prisma.monitoramento.findMany();
  }

  async delete(id: number): Promise<Monitoramento> {
    return prisma.monitoramento.delete({ where: { id } });
  }
}
