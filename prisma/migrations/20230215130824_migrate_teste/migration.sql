-- CreateTable
CREATE TABLE "Monitoramento" (
    "id" SERIAL NOT NULL,
    "hdUsado" DOUBLE PRECISION,
    "cpuUso" DOUBLE PRECISION,
    "memUsada" DOUBLE PRECISION,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,

    CONSTRAINT "Monitoramento_pkey" PRIMARY KEY ("id")
);
