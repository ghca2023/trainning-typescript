import express, { Request, Response } from "express";
import dotenv from "dotenv";

dotenv.config();

const app: express.Application = express();
const port: string | number = process.env.PORT || 3000;

app.get("/", (req: Request, res: Response) => {
  res.send("Express + TypeScript Server");
});
app.listen(port, () => {
  console.log(`[server]: Server is running at http://localhost:${port}`);
});
